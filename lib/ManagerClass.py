'''
Created in May 2016

Author: Cristovao Cordeiro <cristovao.cordeiro@cern.ch>

Copyright (C) 2016 CERN
'''

from .VMsClass import VM
from novaclient import exceptions

class Manager():
    """The manager class with the provisioning configurations and actions

    Attributes:
        connection: the connection to Openstack
    """

    def __init__(self, connection, soap_conn, provisioning_config, LOGGER):
        """Return the Manager object with its respective Openstack connection
        and provisioning parameters"""
        self.soap_conn = soap_conn
        self.connection = connection
        self.config = provisioning_config
        self.LOGGER = LOGGER

    def list_vms(self):
        """List all the VMs running on this OS_TENANT_NAME. Returns a list of
        all the VM objects found"""
        all_vms = self.connection.servers.list()

        my_vms = []
        for vm in all_vms:
            if vm.name.startswith(self.config["vmbasename"]):
                vm_obj = VM(self.connection, vm, self.LOGGER)
                if vm.status == "ERROR":
                    try:
                        vm_obj.delete_me()
                    except exceptions.NotFound as e:
                        LOGGER.info("VM %s has already been deleted." % vm.name)
                else:
                    if vm_obj.get_pnode(self.soap_conn):
                        my_vms.append(vm_obj)

        return my_vms

    def boot_vms(self, hostname, nVMs):
        """Issues a nova boot request for max_count VMs called hostname"""
        #Check if image_name/flavor exist and get corresponding objects
        image_obj = self.connection.images.find(id=self.config["imageid"])
        flavor_obj = self.connection.flavors.find(name=self.config["flavor"])

        try:
            with open(self.config["userdatafile"], "r") as ud_file:
                userdata_content = ud_file.read()
        except IOError:
            self.LOGGER.error("!! Userdata file was not found. Moving on... !!")

        try:
            self.connection.servers.create(hostname, image_obj, flavor_obj, \
                        { 'automanaged' : 'true', 'cern-services' : 'false' }, \
                        userdata=userdata_content.strip().replace('\t',''), \
                        key_name=self.config["keyname"], max_count=nVMs)

            self.LOGGER.info("Booting %s VMs as %s" % (nVMs, hostname))
        except exceptions.BadRequest as e:
            self.LOGGER.exception("Hostname ERROR while booting %s VMs" % nVMs)
        except exceptions.Forbidden as e:
            if "Quota exceeded" in e.message:
                self.LOGGER.exception("Quota exceeded was raised. Retry next time")
            else:
                self.LOGGER.exception("Unkown FORBIDDEN while booting VMs")
