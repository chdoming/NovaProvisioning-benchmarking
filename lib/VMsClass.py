'''
Created in May 2016

Author: Cristovao Cordeiro <cristovao.cordeiro@cern.ch>

Copyright (C) 2016 CERN
'''

import dateutil.parser
import datetime
import pytz

VM_TIMEOUT = 3600   # 1 hour

class VM(object):
    """The VM properties as they come from Openstack

    Attributes:
        name: A string containing the hostname of the VM, used as identifier
    """

    def __init__(self, connection, vm, LOGGER):
        """This constructor expects the nova connection client and a nova
        VM object"""
        self.connection = connection
        self.vm_inst = vm
        self.LOGGER = LOGGER
        self.pnode = "n/a"

    def get_pnode(self, soap_conn):
        """Get the physical node hosting this VM"""
        if self.vm_inst.status == "ACTIVE":
            try:
                self.pnode = soap_conn.vmGetInfo(self.vm_inst.name).VMParent
                return True
            except:
                self.vm_inst.delete()

        return False

    def delete_bad_vm(self):
        """Check the status of the VM and act accordingly"""
        current_time = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
        vm_created_time = dateutil.parser.parse(self.vm_inst.created)

        if self.vm_inst.status == "SHUTOFF" or self.vm_inst.status == "ERROR" \
                or self.vm_inst.status == "DELETED" \
                or ((current_time - vm_created_time).total_seconds() > VM_TIMEOUT \
                    and self.vm_inst.status != "ACTIVE"):
            self.LOGGER.warn("%s is not ACTIVE, deleting..." % self.vm_inst.name)
            self.vm_inst.delete()
            return True
        else:
            return False

    def delete_me(self):
        """Deletes itself"""
        self.LOGGER.info("Deleting %s in HV %s" % (self.vm_inst.name, self.pnode))
        self.vm_inst.delete()

    def is_active(self):
        """True if VM has status=ACTIVE"""
        if self.vm_inst.status == "ACTIVE":
            return True
        else:
            return False

    def __del__(self):
        """Destructor"""
        #self.LOGGER.warn("%s object has been closed" % self.vm_inst.name)
