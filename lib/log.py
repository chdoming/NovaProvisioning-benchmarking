'''
Created in May 2016

Author: Cristovao Cordeiro <cristovao.cordeiro@cern.ch>

Copyright (C) 2016 CERN
'''

import logging
import logging.handlers
import sys

def setup_custom_logger(name, filename):
    formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')

    filehandler = logging.handlers.RotatingFileHandler(filename, maxBytes=50000000, backupCount=10)
    filehandler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(filehandler)
    logger.propagate = False

    root = logging.getLogger()
    root.setLevel(logging.ERROR)
    chandler = logging.StreamHandler(sys.stdout)
    chandler.setFormatter(formatter)
    root.addHandler(chandler)

    return logger
