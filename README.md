# Description

This repository is meant for internal use only, as it relies on specific services
and endpoint only available within CERN.

The _nova_resource_manager.py_ script will load the user configuration from
the __.ini__ file and provision VM in OpenStack@CERN accordingly.
This script expects the proper project environment to be setup prior to its
execution.

The provisioned resources are contextualized to periodically run a specific
set of benchmarks, using a 3rd party benchmark suite.

### Provisioning model

When executed, the code will run 1 cycle where it attempts to boot the amount
of requested VMs while checking their health state and making sure they match
the requested ratio VMs per hypervisor.
More details on the following workflow:
![System workflow](workflow.png)


### The .ini configuration file

```yaml
[SectionName]
VMBasename: basename          # basename for the VMs
ImageID: IMAGEID              # OS for the VM
UserDataFile: path/bmk_ud     # path for the user-data file
MaxVMs: 14                    # max N VMs to provision, cannot boot more
Flavor: flavor_name           # VM flavor
VMsPerHypervisor: 2           # how many VMs per hypervisor
NumHypervisors: 7             # how many HVs. 2*7 = 14
KeyName: ssh_key              # key for SSH
cleanup_all: false            # delete all VMs if true
```
