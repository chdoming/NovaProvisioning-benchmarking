#!/usr/bin/env python
'''
Created in May 2016

Author: Cristovao Cordeiro <cristovao.cordeiro@cern.ch>

Copyright (C) 2016 CERN

Automatic provisioner for the CERN Openstack IaaS.

Uses the novaclient to interact with the infrastructure and simply boots,
checks and deletes virtual machines according to the user configurations
specified on an external configuration file nova_resource_manager.ini.

Prior to its execution, it expects the Openstack credentials to be already
loaded into the system's environment.
'''

import lib.log as log
import os
import signal
import sys
import logging
import argparse
import ConfigParser
import SOAPpy
import socket
import uuid
from novaclient import client
from novaclient import exceptions
from lib.VMsClass import VM
from lib.ManagerClass import Manager

__author__ = "Cristovao Cordeiro"
__copyright__ = "Copyright (C) 2016 CERN"

__maintainer__ = "Cristovao Cordeiro"
__email__ = "cristovao.cordeiro@cern.ch"
__status__ = "Test"


LOCK_FILE = '/tmp/nova_resource_manager.lock'
LOG_FILE = '/tmp/nova_resource_manager.log'

LOGGER = log.setup_custom_logger(__name__, LOG_FILE)

def delete_file(filename):
    """ For any filename, verifies if it exists and delete it. """
    LOGGER.info(" ## Running in %s" % sys._getframe().f_code.co_name)

    if os.path.isfile(filename):
        os.remove(filename)


def signal_handler():
    """ A signal handler to be used for program interruptions. """
    LOGGER.info(" ## Running in %s" % sys._getframe().f_code.co_name)

    LOGGER.warning('SIGINT, exiting.')
    print ' -- User interruption -- '
    delete_file(LOCK_FILE)
    sys.exit(1)


def init():
    """ Initialize the application runtime settings, including signal
    handling and argparsing. """
    LOGGER.info(" ## Running in %s" % sys._getframe().f_code.co_name)

    #Catch CTRL+C (SIGINT)
    signal.signal(signal.SIGINT, signal_handler)

    parser = argparse.ArgumentParser(description='Nova custom manager')
    parser.add_argument('-c', '--config', action='store',
                        default='nova_resource_manager.ini', \
                        metavar='configFile', dest='configFile', \
                        help='configuration file to be used. If not specified, \
                                it will try %(default)s.')

    params = parser.parse_args()
    return params.configFile


def lockPID():
    """ Populated the LOCK_FILE with the processe's PID """
    try:
        pidfile = open(LOCK_FILE, "r")
        pidfile.seek(0)
        previous_pid = pidfile.readline()

        if os.path.exists("/proc/%s" % previous_pid):
            LOGGER.info("Instance of the program is already running (%s)" % previous_pid)
            sys.exit()
        else:
            LOGGER.warn("%s exists but the program is not running. Removing it" % LOCK_FILE)
            delete_file(LOCK_FILE)
    except IOError:
        LOGGER.info("The file %s does not exist...creating it" % LOCK_FILE)
    except:
        raise

    with open(LOCK_FILE, "w") as create_pid_file:
        create_pid_file.write("%s" % os.getpid())


def parse_config(filename):
    """ Reads and parses the configuration file into a data structure """
    LOGGER.info(" ## Running in %s" % sys._getframe().f_code.co_name)
    Config = ConfigParser.ConfigParser()
    Config.read(filename)
    cfgs = {}
    # TODO: this will only consider the LAST SECTION on the ini file
    for section in Config.sections():
        options = Config.options(section)
        for option in options:
            try:
                cfgs[option] = Config.get(section, option)
            except:
                print("exception on %s!" % option)
                cfgs[option] = None
    return cfgs


def SOAP_client(username, password):
    """ Initializes the SOAP connection """
    LOGGER.info(" ## Running in %s" % sys._getframe().f_code.co_name)
    endpoint = "https://network.cern.ch/sc/soap/soap.fcgi?v=5"
    net_service = "http://network.cern.ch/NetworkService"
    SOAPserver=SOAPpy.SOAPProxy(endpoint, namespace=net_service)
    #Get the auth token
    atoken=SOAPserver.getAuthToken(username,password,"NICE")
    #Build the auth header
    authStruct=SOAPpy.structType(data = {"token" :atoken})
    authStruct._ns1=("ns1","urn:NetworkService")
    authHeader=SOAPpy.headerType(data = {"Auth":authStruct})
    SOAPserver.header=authHeader
    return SOAPserver


def manage_nodes(mgr_obj):
    """ Handles the VM objects, deleting, checking and creating new ones """
    LOGGER.info(" ## Running in %s" % sys._getframe().f_code.co_name)

    # Let's first check in which pnode each VM is
    my_vms_obj = mgr_obj.list_vms()

    if mgr_obj.config.has_key("cleanup_all") and \
            mgr_obj.config["cleanup_all"].lower() == "true":
        for vm in my_vms_obj:
            vm.delete_me()

        return

    # If the number of VMs is below the MaxVMs, then boot more
    max_nVMs = int(mgr_obj.config['maxvms'])
    goal_nVMs = int(mgr_obj.config['vmsperhypervisor']) * int(mgr_obj.config['numhypervisors']) \
                    if int(mgr_obj.config['vmsperhypervisor']) * int(mgr_obj.config['numhypervisors']) <= max_nVMs \
                    else max_nVMs

    if goal_nVMs < max_nVMs:
        LOGGER.error("MaxVMs needs to be at least equal to NumHypervisors*VMsPerHypervisor")
        return
    initial_nVMs = len(my_vms_obj)
    LOGGER.info(" -- Total VMs: %s" % initial_nVMs)

    vms_per_hypervisor = {}
    removed_vms = 0
    non_active_VMs = []
    for vm in my_vms_obj:
        # Double check for VMs stuck in ERROR/DELETING
        try:
            if vm.delete_bad_vm():
                removed_vms += 1
                #my_vms_obj.remove(vm)
        except exceptions.NotFound as e:
            LOGGER.info("VM %s seems to have been deleted already...continuing" % vm)

        # If VMs are not ACTIVE, there's no point continuing cause we can't check their HV
        if not vm.is_active():
            non_active_VMs.append(vm)
            pass

        if vm.pnode in vms_per_hypervisor.keys():
            #print vm.pnode,len(vms_per_hypervisor[vm.pnode]), mgr_obj.config['vmsperhypervisor']
            if len(vms_per_hypervisor[vm.pnode]) >= int(mgr_obj.config['vmsperhypervisor']):
                print vms_per_hypervisor[vm.pnode], vm.vm_inst.name
                vm.delete_me()
                removed_vms += 1
                #my_vms_obj.remove(vm)
            else:
                #print vm.pnode,len(vms_per_hypervisor[vm.pnode]), mgr_obj.config['vmsperhypervisor']
                vms_per_hypervisor[vm.pnode].append(vm.vm_inst.name)
        else:
            if mgr_obj.config['vmsperhypervisor'] > 0:
                vms_per_hypervisor[vm.pnode] = [vm.vm_inst.name]

    LOGGER.info("Current VMs per hypervisor: %s" % vms_per_hypervisor)

    final_nVMs = len(my_vms_obj)-removed_vms
    LOGGER.info(" -- Total VMs after cleanup: %s" % final_nVMs)

    if len(non_active_VMs) > 0:
        LOGGER.warn('    %s VMs are not ACTIVE. Their PNODE cannot be retrieved. \
                        Please retry provisioning later.' % len(non_active_VMs))

        return


    if final_nVMs < goal_nVMs:
        missing_nVMs = goal_nVMs - final_nVMs
        LOGGER.warn("There are still %s VMs missing to reach the goal" % missing_nVMs)

        if missing_nVMs == 1:
            vm_hostname = mgr_obj.config['vmbasename'] + '-' + str(uuid.uuid4())
        else:
            vm_hostname = mgr_obj.config['vmbasename']

        mgr_obj.boot_vms(vm_hostname, missing_nVMs)


if __name__ == '__main__':
    LOGGER.info('''
        |\ | _    _    _ _  _  _  _  _  _  _
        | \|(_)\/(_|  | | |(_|| |(_|(_|(/_|
                                     _|''')
    try:
        config_file = init()
        lockPID()
        configs = parse_config(config_file)
        LOGGER.info("Provisioning with the following config: %s" % configs)
        LOGGER.info("Connecting to CERN Openstack...")
        conn = client.Client("2", os.environ.get('OS_USERNAME'), \
                            os.environ.get('OS_PASSWORD'), \
                            os.environ.get('OS_TENANT_NAME'), \
                            os.environ.get('OS_AUTH_URL'), \
                            service_type="compute", insecure=True)

        LOGGER.info("Connecting to Network Service@CERN...")
        soap_conn = SOAP_client(os.environ.get('OS_USERNAME'), \
                                os.environ.get('OS_PASSWORD'))

        # Construct the Manager class with the above information
        my_manager = Manager(conn, soap_conn, configs, LOGGER)

        manage_nodes(my_manager)
    except (KeyboardInterrupt, SystemExit):
        delete_file(LOCK_FILE)
        logging.shutdown()
        sys.exit(0)
    except:
        LOGGER.exception("unknown error")

    # Cleanup
    delete_file(LOCK_FILE)
    LOGGER.info('Finished provisioning cycle')
    logging.shutdown()
